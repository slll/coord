# Propuestas ser la sede de esLibre

 * **2019**:
     * [Almería](2019/Almería/README.md)
     * [Granada](2019/Granada/README.md) (_presencial/seleccionada_)


 * **2020**:
     * [Madrid](2020/Madrid/README.md) (_virtual/seleccionada_)

 * **2021**:
    * [CONVOCATORIA DE SEDE ESLIBRE 2021](2021/README.md)
    * [_TU PROPUESTA_](2021/plantilla.md)
