# Convocatoria de Sede esLibre 2021

esLibre es un encuentro de personas interesadas en las tecnologías libres, enfocado a compartir conocimiento y experiencia alrededor de las mismas: todo con la intención de crear comunidad. La primera edición tuvo lugar en Granada el 21 de Junio de 2019 con unas 50 actividades entre charlas, talleres y mesas redondas repartidas por varios tracks y devrooms (<https://eslib.re/2019/programa/>).

La segunda edición tuvo lugar de forma virtual en Madrid durante los días 18 y 19 de septiembre de 2020, con más de 200 asistentes en determinados momentos y más de 40 actividades entre charlas, talleres y salas. Queremos volver a empezar la planificación para el año que viene, y es por eso que al igual que en años pasados lanzamos esta **"Convocatoria de Sede"** con el objetivo de recibir propuestas para **esLibre 2021**.

Como novedad para 2021, las candidaturas pueden decidir proponer un evento físico o virtual. Sigue leyendo para ver cómo hacer una propuesta para organizar esLibre en 2021.

## Información necesaria:

### Equipo organizador

El propósito de esta convocatoria es que se presenten grupos de personas que den forma a propuestas para coordinar de forma voluntaria la organización local o virtual del evento, por lo que para empezar, será imprescindible indicar las personas que compondrían dicho equipo.

En esLibre no existe una _cúpula_ dirigente, pero sí que existe un grupo organizador _general_ que son las personas que estuvieron relacionadas con la organización de la pasada edición y que han presentado su interés en seguir participando activamente en las cuestiones organizativas.

Las candidaturas presentadas deberían indicar la experiencia previa tanto en la organización de actividades de la misma temática, así como la participación en comunidades relacionadas con la difusión de las tecnologías libres y el conocimiento abierto. De igual modo, si se pudiera explicar a modo resumen la motivación a la hora de organizar un evento de este tipo sería de agradecer.

### Formato del congreso

Aunque la temporización y la diversificación de las actividades se deja a criterio del equipo organizador local, sí que la estructura del evento debe seguir el mismo esquema que en pasadas ediciones:

-   Una serie de salas principales que serían gestionadas integramente por la organización local, contando para ello también con el apoyo del equipo coordinador general
-   Posibilidad de salas organizadas por diferentes comunidades, comprometiéndose estas a coordinarse con la organización local para intentar que logísticamente todas las actividades sean lo más homogéneas posibles (pero comprendiendo que en todo momento se respetara su forma para las mismas)
-   Sesiones plenarias de apertura y cierre del congreso con todos los asistentes, participantes y organizadores

El grupo organizador que realiza la propuesta debe decidir si organizará el congreso de forma presencial o virtual. En el primer caso, se debe justificar adecuadamente la disponibilidad de espacios para el correcto funcionamiento del congreso. En el segundo caso, se debe describir la infraestructura técnica disponible y el software que se planea utilizar y si existen experiencia previa en el despliegue, operación y uso de dicho software.

Es importante tener en cuenta que posiblemente el público podría ser diferente en el caso de un evento virtual y otro presencial, por tanto podría elegirse dentro del mismo año una edición presencial y una a distancia del congreso. Obviamente, se pedirá a ambas organizaciones locales cierta flexibilidad para que no estén demasiado cercanas en el tiempo. Es por ello que animamos a que se realicen propuestas con independencia de que ya hubiera otras en otra modalidad diferente.

### Posibles fechas

Preferiblemente en los meses de **mayo-junio-julio**. Indicar también si hay la **posibilidad de que el evento se desarrolle durante fin de semana** (total o parcialmente) y si se tiene intención de que sea un único día o varias jornadas.

Por otra parte, se deberá intentar evitar por todos los medios que la fecha elegida sea, al menos en proximidad de días, cercana a la de cualquier otro evento de temática similar. Otro de los puntos importantes de las comunidades es el intercambio de puntos de vista, y una gran forma de conseguir eso es que haya un amplio número de oportunidades de hacerlo, por lo que no entorpecernos en la organización de eventos pensados con tal fin es algo que debería tener un prioridad especial.

### Presupuesto económico

La intención es que el evento siga siendo de asistencia gratuita. Como conocemos la dificultad de llevar a cabo un evento así sin coste alguno para el público, también se deberá indicar si el propio equipo organizador local tendría acceso a algún tipo de financiación que pudieran invertir en una mejora de alguna cuestión del evento, bien mediante un patrocinio general (que en el caso de ser por una empresa, esta debería no deberá tener una principal actividad que choque frontalmente con la filosofía del evento) o presupuestos/subvenciones de instituciones públicas.

Si existiera la posibilidad de cubrir este punto, comentar en qué se invertiría ese presupuesto: café y/o comida, packs de bienvenida (camisetas, acreditaciones, recuerdo local...), cartelería y medios físicos de difusión, grabación y retransmisión de las actividades...

### Facilidades disponibles

Hay ciertas capacidades que o bien son imprescindibles para llevar a cabo el evento o, por otra parte, facilitarían de forma considerable la asistencia del público interesado. Es por esto que la organización del evento debería aclarar si se tienen asumidas en el momento de presentar la candidatura, ya que por ejemplo en el caso de una candidatura presencial, no se podría dar como válida si no tiene un lugar físico asegurado para el desarrollo del congreso (por eso es el primer punto a destacar):

-   Disponibilidad de lugar físico para el desarrollo del congreso
-   Posibilidad de catering
-   Posibilidad de descuentos en transporte desde los diferentes puntos del país
-   Posibilidad de alojamientos asequibles o descuentos cercanos a la sede del evento

Por otro lado, una candidatura virtual, no se podría dar por válida si no cuenta con la infraestructura necesaria para hacerla posible:

-   Disponibilidad de hardware para el despliegue de las soluciones técnicas para el desarrollo del congreso
-   Descripción de las soluciones técnicas elegidas (por ejemplo, solución para presentaciones online, solución para discusiones y solución para eventos virtuales durante el congreso)
-   Posibilidad de montar reuniones sobre a marcha con participantes en el evento

### Aspectos logísticos

Para facilitar la difusión del evento desde el mismo momento en el que se decida finalmente la sede es importante conocer una serie de detalles que seguramente serán de interés para el público interesado en asistir, además, algunos aspectos también serán de agradecer por los organizadores de futuras ediciones, facilitándoles su labor y ayudando a mantener la identidad que vaya adquiriendo el congreso.

-   Espacios disponibles, así como la facilidad de acceso, en la propia sede para las actividades (número de salas disponibles, capacidad de la mismas, salón para actos de apertura y clausura)
-   Relación con otras comunidades tecnológicas y/o divulgativas que pudieran participar
-   Formato de las actividades: charlas y talleres (cortas/largas/diferentes niveles), uno o varios tracks, tracks específicos por tecnologías, propuestas de devrooms por la comunidad...
-   Medios de transporte disponibles para llegar al lugar de la sede y facilidad para llegar a la propia sede: carretera, autobús, tren...
-   Como se gestionaran las solicitudes de ponencias y las inscripciones de los asistentes
-   Si se considera alguna forma de conciliación de familias asistentes al congreso (talleres de robótica, actividades lúdicas al aire libre...)
-   Si se cuenta con el apoyo de alguna institución
-   Planes para actividades comunitarias fuera del horario del congreso (comida/cena de la comunidad, ruta turística/culturar, salida nocturna...)
-   Informe de valoración post-evento

### Lugar de la sede

En el caso de una propuesta para un evento presencial, hacer un pequeño resumen sobre la ciudad que daría sede a este evento, tanto algunas cosas que la hagan característica como aspectos que puedan hacerla interesante para un evento de estas características y que podría resultar especialmente llamativo a los asistentes que nunca hubieran estado ahí. En el mismo caso, si la propia sede del congreso tiene alguna historia o valor cultural relevante.

### Presentación de candidaturas

Para una mayor transparencia, tenemos intención de seguir un procedimiento similar al de los dos años pasados. Las candidaturas se presentarán mediante un "merge request" para la publicación de un documento en formato Markdown en [la carpeta correspondiente a este año de nuestro repositorio de coordinación en GitLab](https://gitlab.com/eslibre/coord/tree/master/propuestas/2021).

Se recomienda seguir [la plantilla creada para tal fin](https://gitlab.com/eslibre/coord/tree/master/propuestas/2021/plantilla.md). En cualquier caso, esta plantilla se debe considerar solo eso, una recomendación de información mínima que debería presentar cualquier candidatura; por otro lado, toda información que se considere relevante y que sea añadida en información extra, seguramente será de agradecer por la comunidad a la hora de realizar la votación para elegir sede.

Así, el proceso será transparente en todo momento, y si se estima que falta algo de información relevante para valorar dicha candidatura, también se hará público para todo el mundo, de modo que se puedan realizar las modificaciones que se estimen oportunas dentro del periodo establecido para la presentación.

El último día para presentar candidaturas o modificarlas será el **14 de diciembre**, siendo publicada la lista de candidaturas definitiva nos más allá del **16 de diciembre**.

Todas las candidaturas presentadas se someterán a votación pública de la comunidad en los días consecutivos a la presentación de las candidaturas definitivas, y así entre todos podremos elegir el lugar más idóneo para el transcurso de este evento en las mejores condiciones posibles.

### Código de conducta

El código de conducta del evento se puede encontrar en <https://eslib.re/conducta/>. Este Código de Conducta es una copia directa del impresionante trabajo del Open Source Bridge, pero sustituido con nuestra información del evento. El original está disponible en <http://opensourcebridge.org/about/code-of-conduct/> y está liberado bajo una licencia [Creative Commons Atribución-CompartirIgual 4.0 Internacional (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/deed.es).

Para cualquier duda o cuestión estamos totalmente abierto a sugerencias y podéis contactarnos por cualquiera de los que disponemos:
- Correo electrónico: [hola@eslib.re](mailto:hola@eslib.re)
- Matrix: #esLibre:matrix.org
- Telegram: [@esLibre](https://t.me/esLibre)
- Mastodon: [@eslibre@hostux.social](https://hostux.social/@eslibre)
- Twitter: [@esLibre_](https://twitter.com/esLibre_)
