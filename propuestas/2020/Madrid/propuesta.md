# Propuesta para la Celebración de esLibre 2020 en Madrid (URJC)

La Oficina de Conocimiento y Cultura Libres de la Universidad Rey Juan Carlos
(OfiLibre de la URJC) propone organizar el Congreso esLibre 2020, y
para definir su propuesta ha preparado este documento.

## Equipo organizador

El equipo organizador de esLibre 2020 estará coordinado por :

* [Jesús M. González Barahona](https://gsyc.urjc.es/jgb)
* [Francisco Gortázar Bellas](https://gestion2.urjc.es/pdi/ver/francisco.gortazar)

Institucionalmente, la organización estará coordinada por:

* **[OfiLibre de la URJC](https://ofilibre.gitlab.io)**

Además, la organización cuenta con el apoyo de:

* **Vicerrectorado de Calidad, Ética y Buen Gobierno de la URJC**
* **[Escuela Técnica Superior de Ingeniería Informática de la URJC](https://www.urjc.es/etsii)**
* **[Escuela Técnica Superior de Ingeniería de Telecomunicación de la URJC](https://www.urjc.es/etsit)**

Algunas organizaciones de estudiantes de la URJC también han mostrado su interés en colaborar:

* **[RoboTech](https://robotech.etsit.urjc.es/)**
* **[Cosmos](https://cosmos.etsit.urjc.es/)**
* **[Informáticos URJC](http://informaticosurjc.es/quienes-somos/)**

Contamos también con la siguiente colaboración específica:

* **[Wikimedia España](https://wikimedia.es)** que tiene interés en
participar colaborando en la organización de una sesión específica
sobre cultura libre, si la organización de esLibre lo considera conveniente.

### Breve reseña de los coordinadores

Jesús González Barahona trabaja como docente e investigador en la URJC,
y anteriormente en la Universidad Carlos III. Tiene una larga trayectoria
de relación con el mundo del software libre. Ha participado en
numerosas actividades de información y promoción sobre software libre,
ha investigado sobre la producción de software libre, ha participado
en proyectos de desarrollo de software, y la creación y puesta en marcha
de empresas con modelos basados en software libre. Tuvo la suerte
de participar en el [I Congreso de Hispalinux](https://web.archive.org/web/20041221060001/http://congreso.hispalinux.es/congreso1998/)
como ponente,
y en participar en la coordinación de tres Congresos de Hispalinux,
dos en la Universidad Carlos III, y otro en la Universidad Rey
Juan Carlos. Ha participado como ponente en numerosos congresos
de la comunidad del software libre, pero también industriales y
académicos, y tiene amplia experiencia en la organización de
congresos, talleres y jornadas.

Francisco Gortázar es docente e investigador en la URJC y ha trabajado anteriormente
en diversas empresas del sector IT. Desde sus inicios en el mundo profesional ha
estado interesado en el software libre, impartiendo cursos y seminarios sobre
tecnologías libres como Git, Docker, Kubernetes o Spring.
Actualmente coordina el proyecto europeo ElasTest cuyo software es liberado bajo
licencia libre, y co-lidera el proyecto de software libre OpenVidu.
En el pasado ha desarrollado otros productos software libre como plugins para el
entorno de desarrollo Eclipse.
Ha participado en congresos de la comunidad software libre y tiene una amplia
experiencia en organización de talleres y seminarios.

### Nuestra motivación

Desde la OfiLibre de la URJC estamos interesados en promover el conocimiento del
software libre, como una parte muy importante de la cultura libre.
Organizar esLibre 2020 nos dará la oportunidad de realizar una acción muy importante
en esta dirección, apoyando a la vez una iniciativa que nos gustaría que durase en el
tiempo, siendo un lugar de reunión estable para la comunidad de software libre.

Además, dado que nuestros intereses incluyen la cultura libre en general,
y teniendo en cuenta que muchas de las personas interesadas en software libre lo
están también en la cultura libre, nos gustaría aprovechar la oportunidad para
abrir esLibre al mundo más amplio de la cultura libre, sin perder por eso su principal razón de ser.

Por último, nos gustaría recuperar la tradición de la URJC de organizar eventos
relacionados con software libre, en cuyo contexto se organizó, hace ya muchos años,
uno de los últimos Congresos de Hispalinux.

### Nuestra experiencia

Tanto los coordinadores principales como las instituciones que nos dan su apoyo
tenemos amplia experiencia en la organización de congresos y eventos de este tipo.
Como ya se comenta en la biografías, hemos estado directamente implicados en
la organización de Congresos de Hispalinux, tanto en la URJC como en la UC3M
([VI Congreso Hispalinux en la URJC](https://web.archive.org/web/20031206073609/http://congreso.hispalinux.es/edicion/2003/presentacion/),
[III Congreso Hispalinux en la UC3M](https://web.archive.org/web/20041216052901/http://congreso.hispalinux.es/congreso2000/),
[II Congreso Hispalinux en la UC3M](https://web.archive.org/web/20041216075334/http://congreso.hispalinux.es/congreso1999/)).
Pero además tenemos amplia experiencia en la organización de congresos nacionales
e internacionales, tanto académicos como industriales, muchos de ellos orientados
al software libre o a las tecnologías libres en general.

Somos también apasionados del modelo de congreso abierto, estilo FOSDEM,
y tenemos mucha experiencia en este tipo de congresos, cuyo espíritu nos gustaría
plasmar en esLibre 2020.

Institucionalmente, la URJC ha organizado gran cantidad de eventos de todo tipo,
y tendremos a nuestra disposición gran parte de esa experiencia, gracias a la
colaboración institucional de Vicerrectorados, Escuelas Técnicas, Asociaciones de Alumnos,
y otras organizaciones de la propia Universidad.

### Sobre la OfiLibre y la URJC

La [Universidad Rey Juan Carlos](https://www.urjc.es) es una Universidad pública,
fundada en 1996, con campus en los municipios de Móstoles, Fuenlabrada, Alcorcón, Madrid y Aranjuez.
Con más de 40.000 alumnos, ofrece más de 60 grados en todas las áreas de conocimiento,
y también programas de máster y doctorado. Entre otros centros, son de especial relevancia
para esta propuesta la [Escuela Técnica Superior de Ingeniería Informática](https://www.urjc.es/etsii),
radicada en Móstoles, con docencia en los campus de Móstoles y Madrid (Vicálvaro) y
la [Escuela Técnica Superior de Ingeniería de Telecomunicación](https://www.urjc.es/etsit),
con docencia en los campus de Fuenlabrada y Alcorcón.

La [Oficina de Conocimiento y Cultura Libre](https://www.urjc.es/ofilibre) (OfiLibre) de la URJC,
 dependiente del Vicerrectorado de Calidad, Ética y Buen Gobierno,
 tiene la función de informar, promocionar, coordinar y facilitar en sus ámbitos de actuación,
 y en colaboración con todos los agentes de la Universidad.

En la OfiLibre queremos ayudar a la comunidad universitaria a comprender la cultura libre,
la publicación libre, los datos abiertos y el software libre.
Para ello nos dedicamos a explicar qué son, cómo funcionan, qué características tienen,
y qué beneficios pueden producir, de forma que cualquiera pueda decidir cómo le conviene
relacionarse con ellos.

La OfiLibre es una iniciativa que trata de contar con la participación de toda la comunidad universitaria
para que, entre todos, podamos encontrar nuestro camino en el mundo del conocimiento y la cultura libres.

### Resumen de entidades colaboradoras o promotoras

* Vicerrectorado de Calidad, Ética y Buen Gobierno de la URJC

  ![URJC](logos/urjc.png)

* OfiLibre de la URJC

  ![OfiLibre](logos/ofilibre.png)

* ETSII de la URJC

  ![ETSII](logos/etsii.png)

* ETSIT de la URJC

  ![ETSIT](logos/etsit.png)

* RoboTech

  ![RoboTech](logos/robotech.png)

* Cosmos

  ![Cosmos](logos/cosmos.jpg)

* Wikimedia España

  ![Wikimedia España](logos/wikimedia.png)

* Informáticos URJC

  ![Informáticos URJC](logos/informaticos_urjc.png)

## Formato del congreso

El formato del congreso se acordará de forma definitiva con el grupo organizador,
por lo que lo siguiente puede considerarse como una propuesta a debatir.
En cualquier caso, esta propuesta está fuertemente basada en el formato de esLibre 2019.

El formato que se propone está compuesto de:

* Sesiones plenarias. Presentaciones de relevancia especial, orientadas a todos los asistentes.
  Organizadas por la organización general del congreso, en parte mediante convocatoria abierta,
  en parte por invitación.
  Durante su celebración no habrá otras sesiones paralelas ni talleres programados.
* Sesiones paralelas. Presentaciones especializadas, agrupadas temáticamente en "salas" (devrooms). 
  Algunas de estas salas podrán ser propuestas por la organización general, y organizadas
  por ella (quizás co-organizadas con otras instituciones), pero en general serán el
  fruto de una "convocatoria de salas", abierta, a la que se presentarán grupos que quieran
  organizarlas. Los grupos que sean elegidos tras esa convocatoria se encargarán
  (normalmente mediante convocatoria abierta, pero también en algunos casos mediante invitación)
  de organizar el programa de cada sala, y de gestionarlas.
  Habrá hasta un máximo de 10, posiblemente entre 6 y 8 sesiones paralelas
  simultáneas.
* Talleres. Hasta un máximo de 4 (pero dependiendo del campus de celebración, quizás sólo uno o dos) talleres simultáneos.
* Mesas de comunidad. En principio, hasta un máximo de 8 (aunque probablemente se podría ampliar).
  Cada mesa estaría dedicada a un proyecto o a un grupo.
  
En cuanto a duración, se proponen dos días, viernes y sábado (ver apartado "Posibles fechas").

Se propone también organizar algunos premios, en la medida de lo posible
por votación de los asistentes. Habrá también uno, elegido por el comité
organizador (o por algún otro mecanismo similar), la "Mención especial del Congreso".

### Sesiones plenarias

Habrá al menos:

* Sesión de apertura (viernes por la mañana). Se tratará de invitar a un ponente
relevante para que realice la apertura del congreso.

* Sesión de cierre (sábado por la tarde). Se hará un cierre del congreso, incluyendo
la entrega de premios y el reconocimiento a los colaboradores y ponentes.

* Otras sesiones plenarias. Algunas de las ponencias recibidas en respuesta
  a la convocatoria de ponencias se seleccionarán como sesiones plenarias.

### Salas (devrooms)

Cada sala tendrá su propio comité organizador, que se encargará del programa de la sala.
La organización planificará también ponencias recibidas en respuesta a la convocatoria
de ponencias en una o varias salas (alguna de ellas podrá ser la sala donde tengan
lugar las sesiones plenarias, pero ya en modo paralelo).

Se espera organizar al menos una sala "general" (organizada por el comité general),
y una sala sobre cultura libre co-organizada con Wikimedia España.

Además, habrá dos salas (no necesariamente funcionando durante todo el tiempo)
con propósitos específicos:

* Presentaciones relámpago (lightning talks): presentaciones cortas, elegidas entre
  las que se presenten a la convocatoria de ponencias, o por invitación.
* Descongreso: presentaciones o grupos de debate que se registren durante el propio
  congreso, o en cualquier caso sin proceso de selección previo (salvo encajar en la
  temática del congreso).

### Talleres

Se ofrecerá la posibilidad de organizar talleres, con un modo de organización
similar al de las salas, sobre tecnologías libres. Las diferencias fundamentales
con las salas serán:

* Los talleres estarán orientados a "manos en la masa", donde los asistentes realicen
trabajo propio durante el taller.
* Dispondrán de salas con ordenadores, si el trabajo a realizar es en ordenador.
* Tendrán una mayor duración, de forma que de tiempo a las exposiciones necesarias,
  y a que los asistentes hagan su trabajo.

### Mesas de comunidad

Se ofrecerá una mesa a los proyectos, y grupos de comunidad, que quieran
disponer de ella. Las mesas se ofrecerán por periodos de un día (o de ambos días).
Los proyectos o grupos que gestionen las mesas se comprometerán a
tener voluntarios siempre presentes en ellas (excepto durante las sesiones
plenarias), y a interactuar con los asistentes para mostrarles su proyecto
o su grupo.

### Convocatorias

* Convocatoria de salas y talleres. Convocatoria abierta a cualquier grupo que quiera
  organizar una sala o un taller. Estos grupos tendrán que preparar una propuesta,
  que será evaluada por el grupo organizador, que elegirá las que finalmente se lleven a cabo.

* Convocatoria de ponencias y mesas. Convocatoria abierta a cualquier persona o grupo de personas
  que quieran proponer una presentación plenaria o en las salas gestionadas por la organización,
  o una mesa de comunidad.
  Cada ponencia podrá proponerse como ponencia plenaria, o como ponencia relámpago,
  pero la organización decidirá en qué formato se acepta (que podría ser otro, distinto
  del propuesto). Si se acepta en un formato distinto del propuesto, quien propuso podrá
  aceptar o no.
  
Las convocatorias se gestionarán mediante merge requests a un repositorio en GitLab,
o procedimiento similar. Las propuestas serán públicas desde el momento en que se hagan,
y podrán ser comentadas por cualquiera. Todas las propuestas tendrán al menos dos
comentarios por el equipo organizador. Finalmente, según los comentarios recibidos,
las propuestas se aceptarán o no. Los comentarios serán también públicos.
  
> [Sí] Posibilidad de participación de otras comunidades mediante organización de devrooms

> [Sí] Espacio para sesiones plenarias de apertura y cierre del congreso con todos los asistentes, participantes y organizadores

## Fechas

Las fechas preferibles para la celebración del congreso 
son los días 5 y 6 de junio, aunque habría también posibilidades de
celebrar el congreso los días 29 y 30 de mayo, o quizás en otras fechas.
En cualquier caso, se propone un esquema de dos días, viernes y sábado,
para dar oportunidades tanto a quien quiera asistir en tiempo de trabajo,
como para quien prefiera (o tenga que) hacerlo en tiempo de ocio.

Los dos días podrían ser en el mismo campus, o en campus distintos.

> Fechas estimadas para la realización del congreso: (ver más arriba)

> [Sí] Posibilidad de que el evento se desarrolle en fin de semana (total o parcialmente)

> [Sí] Nos comprometemos en la medida de lo posible a evitar que la fecha final del congreso choque con la de otro congreso de temática similar.
>
> Por ahora, no somos conscientes de otros congresos de temática relacionada en esas fechas.

Otras fechas:

* Convocatoria de ponencias, salas, talleres y mesas: enero
* Elección de ponencias, salas, talleres y mesas: marzo
* Programa disponible: abril
* Informe final: julio

## Presupuesto económico

La asistencia al congreso será libre y gratuita. La URJC correrá con cualquier
gasto derivado de la apertura de los campus para la celebración del congreso,
y del normal uso de su infraestructura.

Los ponentes y los grupos organizadores (de salas, de talleres, etc.) en general no recibirán contraprestaciones
económicas por su participación en el congreso.

Los colaboradores en la organización del congreso no recibirán contraprestaciones
económicas por esta participación.

Se dispondrá de una pequeña bolsa de presupuesto, aportada por la URJC, que podría cubrir:

* Ponente invitado. Gastos de desplazamiento y estancia de un ponente invitado relevante, 
  si se tuviera que desplazar desde fuera de la Comunidad de Madrid.
* Premios. Gastos de compra de pequeños reconocimientos a los ganadores de los premios.
* Grabación de ponencias. Gastos de la grabación de las ponencias plenarias, y quizás
  de alguna de las salas. Dependiendo de presupuesto, quizás esto permita también
  la transmisión en streaming de alguna de las ponencias.

Además, se organizará una búsqueda de patrocinios para gastos concretos
(por ejemplo, bebidas de cortesía para ponentes).
En cualquier caso, estos patrocinios no podrán estar ligados a ninguna influencia
sobre el programa, y sólo darán derecho a aparecer como patrocinador
en el sitio web de congreso, y si lo aportan, a un cartel en la sala de sesiones plenarias.

> [Sí] La asistencia al evento será gratuita

> Posible financiación:
>    * [ ] Empresas privadas (nos comprometemos a que no sean por parte de compañías cuya actividad pudiera ser contraria a la filosofía del evento)
>    * [Sí] Presupuestos/subvenciones de instituciones públicas

> Posibles beneficios para el congreso en los que se invertiría el patrocinio:
>
> Gastos concretos, que mejoren la experiencia de asistencia al congreso,
> o que ayuden a reconocer la colaboración de ponentes y otros voluntarios.

## Facilidades disponibles

> [Sí] Disponibilidad de lugar físico para el desarrollo del congreso

El congreso se celebrará en uno de los campus de la URJC. Los tres campus
candidatos son los de Móstoles, Fuenlabrada y Vicálvaro. Cada uno tiene
sus propias ventajas, dependiendo de fechas exactas y de orientación del
congreso. El campus de Vicálvaro es el de mejor disponibilidad según fechas
(estaría disponible con seguridad durante todas las fechas propuestas), y
 está más cerca del centro de Madrid y del aeropuerto. El campus de
 Móstoles dispone de una sala de alta capacidad (unas 500 personas), que
 podría ser usada según las fechas. El campus de Fuenlabrada es el
 que ofrece una oferta más compacta (todo podría estar básicamente en el
 mismo edificio), y unas mejores opciones de laboratorios (ya instalados
 con software libre) en caso de que se quieran utilizar para talleres
 prácticos. Los campus de Fuenlabrada y Vicálvaro están más imbricados
 en sus respectivas poblaciones, lo que ofrece más oportunidades, por ejemplo,
 de restauración.

![urjc_rectorado](imagenes/mostoles2.jpg)
 
Sin embargo, en líneas generales estas diferencias entre campus son menores.
Todos ellos ofrecen un buen conjunto de sala grande para sesiones plenarias 
y salas medianas para sesiones en paralelo. En todos ellos hay opciones para
talleres prácticos. Todos están bien comunicados por transporte público
(todos tienen una parada de metro junto al campus), y todos disponen de
aparcamiento. En todos habrá oportunidades para restauración. Por lo tanto,
pensamos que la decisión sobre la sede exacta se tomará mejor de acuerdo con
la organización de esLibre, teniendo en cuenta fechas y otros factores.
Cabe incluso la posibilidad de realizar cada uno de los dos días en un
campus distinto. Esta propuesta pone a disposición de esLibre los tres
campus, para que se pueda tomar la mejor decisión en su momento.

![urjc_sala](imagenes/mostoles6.jpg)

En resumen, en cualquiera de los campus, se ofrecería al menos:

* Una sala grande (más de 200 personas) para sesiones plenarias
* Un mínimo de 8 salas medianas (20-80 personas) para salas (devrooms).
* Un mínimo de un laboratorio de ordenadores para talleres.
* Un mínimo de una sala para ponentes y organización.
* Un mínimo de una zona para recepción de asistentes.

![urjc_sala2](imagenes/mostoles8.jpg)

### Campus de Móstoles

[Información en el sitio web de la URJC](https://www.urjc.es/universidad/campus/campus-de-mostoles)

![plano3d mostoles](imagenes/mostoles-3d.jpg)
![plano3d mostoles2](imagenes/mostoles5.jpg)

En caso de celebrarse el congreso en este campus, podrían usarse
las siguientes instalaciones:

* Aula Magna, edificio Aulario III, capacidad 244 personas. Esta sala
podría usarse para las sesiones plenarias. Está dotada de proyector,
equipo de sonido, micrófonos, grabación de vídeo y pizarras.

* Aulas, edificio Aulario III, capacidad entre 60 y 100 personas.
Están dotadas de equipo de proyector, pizarras, y en algunos casos,
equipo de sonido y micrófonos. Podremos disponer de al menos 10 de estas
salas, según necesidades. 

* Laboratorios informáticos, probablemente en edificio Laboratorios III, capacidad
entre 40 y 60 personas. Están dotadas de equipos informáticos (normalmente
40 puestos), proyector, y pizarras. Podremos disponer de al menos 2 de estas salas,
según necesidades.

![foto_mostoles](imagenes/mostoles1.png)

El edificio Aulario III y el edificio Laboratorios III están contiguos,
de forma que todas las sesiones del congreso se realizarían en la misma zona,
con periodos de desplazamiento máximos de 2-3 minutos entre las distintas salas.

Quizás estuviera disponible (pendiente de confirmación, y dependiendo de la fecha final):

* Salón de Actos de edificio de Rectorado, capacidad 500 personas,
proyector, equipo de sonido, micrófonos.

### Campus de Fuenlabrada

[Información en el sitio web de la URJC](https://www.urjc.es/universidad/campus/campus-de-fuenlabrada)

![plano3d fuenlabrada](imagenes/fuenla-3d.jpg)
![plano3d fuenlabrada2](imagenes/fuenla4.jpg)

En caso de celebrarse el congreso en este campus, podrían usarse
las siguientes instalaciones:

* Aula Magna, edificio Aulario III, capacidad 234 personas. Esta sala
podría usarse para las sesiones plenarias. Está dotada de proyector,
equipo de sonido, micrófonos, grabación de vídeo y pizarras.

* Aulas, edificio Aulario III, capacidad entre 60 y 100 personas.
Están dotadas de equipo de proyector, pizarras, y en algunos casos,
equipo de sonido y micrófonos. Podremos disponer de al menos 10 de estas
salas, según necesidades. 

* Laboratorios informáticos, probablemente en edificio Laboratorios III, capacidad
entre 40 y 80 personas. Están dotadas de equipos informáticos (normalmente
uno por persona), proyector, y pizarras. Podremos disponer de al menos 4 de estas salas,
según necesidades. Las salas que se usasen tienen instalados sistemas
 Linux, basados en Mint/Ubuntu Más información en el [sitio web de los laboratorios
Linux de la ETSIT](https://labs.etsit.urjc.es/).

![foto_fuenlabrada](imagenes/fuenla2.jpg)

El edificio Aulario III y el edificio Laboratorios III están contiguos,
unidos por un pasillo voladizo,
de forma que todas las sesiones del congreso se realizarían en la misma zona,
con periodos de desplazamiento máximos de 2-3 minutos entre las distintas salas.

Quizás estuviera disponible (pendiente de confirmación, y dependiendo de la fecha final):

* Salón de Actos de edificio de Gestión, capacidad unas 250 personas,
proyector, equipo de sonido, micrófonos.

### Campus de Vicálvaro

[Información en el sitio web de la URJC](https://www.urjc.es/universidad/campus/campus-de-mostoles)

![plano3d vicalvaro](imagenes/vical-3d.jpg)
![plano3d vicalvaro2](imagenes/vical1.png)

En caso de celebrarse el congreso en este campus, podrían usarse
las siguientes instalaciones:

* Salón de Grados, capacidad 259 personas. Esta sala
podría usarse para las sesiones plenarias. Está dotada de proyector,
equipo de sonido, micrófonos, grabación de vídeo y pizarras.

* Aulas, capacidad entre 60 y 100 personas.
Están dotadas de equipo de proyector, pizarras, y en algunos casos,
equipo de sonido y micrófonos. Podremos disponer de al menos 10 de estas
salas, según necesidades. 

* Laboratorios informáticos, capacidad
entre 40 y 60 personas. Están dotadas de equipos informáticos (normalmente
40 puestos), proyector, y pizarras. Podremos disponer de al menos 2 de estas salas,
según necesidades.

![foto_vicalvaro](imagenes/vical6.jpg)

El Salón de Grados, las aulas y los laboratorios están situadas en
edificios contiguos,
de forma que todas las sesiones del congreso se realizarían en la misma zona,
con periodos de desplazamiento máximos de 2-3 minutos entre las distintas salas.

## Aspectos logísticos


> [Sí] Posibilidad de catering de algún tipo

Los tres campus propuestos disponen de cafetería, en la que
también se sirven comidas, bocadillos, etc. En el caso del campus de Vicálvaro,
la cafetería está abierta viernes y sábado (los dos días que se propone organizar
el congreso). En los otros dos campus, está abierta el viernes y se negociaría
 su apertura el sábado, con el apoyo de la Gerencia de los campus correspondientes
 (por lo que no se prevén problemas al respecto).
Además, en los campus de Vicálvaro y Fuenlabrada hay en las cercanías (alrededor
de cinco minutos andando) varios establecimientos de restauración y cafeterías.

Se explorará la opción de patrocinio, bien institucional de la Universidad,
o bien de terceras partes, para poder ofrecer café y quizás catering o vales
de comida a los
asistentes, pero en el momento de redactar esta propuesta, esto es algo que
no se puede garantizar.

> [ ] Posibilidad de descuentos en transporte desde los diferentes puntos del país

Se explorará esta posibilidad, vía los contactos de la URJC con agencias de viaje
y entidades de transporte.

> [Sí] Posibilidad de alojamientos asequibles o descuentos cercanos a la sede del evento

En todos los campus hay hoteles cercanos a precios asequibles, si se realizan
reservas con la antelación suficiente (especialmente en los campus de Fuenlabrada
y Móstoles). Se explorará la aplicación de descuentos mediante los acuerdos que
tiene la Universidad con alguno de estos hoteles.

> Espacios disponibles en la propia sede para las actividades (número de salas disponibles, capacidad de la mismas, salón para actos de apertura y clausura...):
> Mínimo de una sala de alta capacidad (alrededor de 250 personas), 10 de capacidad
> media (40-80 personas), y un laboratorio con ordenadores (40 personas) en todos
> los campus. Detalles en la sección de descripción de los campus.

> [Sí] Los espacios son accesibles

> [Sí] Existe facilidad para llegar con transporte público a la sede del congreso

> Medios de transporte disponibles para llegar al lugar de la sede y facilidad para llegar a la propia sede:
> Todos los campus cuentan con una parada de metro y autobús, y están bien integrados
> en la red de transporte público de la Comunidad de Madrid

Madrid es una de las ciudades mejor comunicadas, tanto con el resto de España
como con el resto de Europa y el mundo:

* Avión: el aeropuerto [Fernando Suárez Madrid-Barajas](https://www.aeropuertomadrid-barajas.com/)
([información oficial](http://www.aena.es/es/aeropuerto-madrid-barajas/index.html))
ofrece conexiones con muchas ciudades españolas, europeas, y de otras partes del mundo.
Los horarios de los vuelos permitirían viajar, desde muchas ciudades de España
 y Europa, la misma mañana de comienzo del congreso,
y realizar el viaje de vuelta durante el misma día de clausura, minimizando el
tiempo de desplazamiento.

* Tren: las dos estaciones principales de Madrid (Atocha y Chamartín) están conectadas
con muchas ciudades españolas por línea de alta velocidad. Esto permite desplazamientos
relativamente cortos en tiempo (en muchos casos, de menos de dos horas y media).
Más información, incluyendo horarios y tarifas, en el [sitio web de RENFE](http://www.renfe.com/).
En muchos casos, esta opción es mejor en tiempo que el avión, aunque su coste
puede en algunos casos ser superior.

* Autobús: Madrid dispone de muchas conexiones de autobús con otras ciudades
españolas, lo que facilitaría un desplazamiento económico, dado que en general
es una opción más barata que el avión o el tren.

* Coche compartido: Madrid es uno de los destinos más populares en sitios web
de viajes en coche compartido. Esta es una opción en general más barata que las
anteriores, y que permitiría además la organización de desplazamientos conjuntos
por varios asistentes al congreso.

* Coche particular: Madrid está conectada mediante autovía y autopista.
Los desplazamientos desde otras partes de la Península son en gran parte menores de
cuatro horas, y raramente de más de seis horas (descansos no incluidos).

Una vez en la Comunidad de Madrid, hay varias opciones para desplazarse
hasta los campus de la URJC:

* Transporte público. Madrid dispone de uno de los mejores sistemas de
transporte público a nivel mundial, con combinación de metro, autobús y tranvía ligero.
Puede consultarse detalles sobre las opciones de transporte en el
[Consorcio de transportes de la Comunidad de Madrid](https://www.crtm.es/muevete-por-madrid.aspx).
En el sitio web del congreso se detallarán las mejores opciones para llegar al
campus de celebración del congreso, incluyendo opciones de abono turístico,
recorridos desde aeropuerto y estaciones de tren de Madrid. En general, los
recorridos en transporte público desde el centro de Madrid hasta los campus son
de unos 30-40 minutos (por ejemplo, desde la Estación de Atocha).
Todos los campus cuentan con una parada de metro.

* Taxis y servicios con conductor. Además de un servicio de taxis, Madrid cuenta
con empresas de servicios con conductor (Uber, Cabify).

* Vehículo particular. Los tres campus están bien conectados mediante autovías
con la red de autovías de la Comunidad de Madrid, con tiempos de desplazamiento desde
el centro de Madrid (M-30) en torno a los 20-25 min. si las condiciones de tráfico
lo permiten. Todos los campus cuentan con amplios aparcamientos.

### Campus de Móstoles

* Dirección: Calle Tulipán s/n. 28933 - Móstoles - Madrid
* En tren: Estación: Móstoles - El Soto C-5. RENFE Cercanías 
* En Metro: Estación: Universidad Rey Juan Carlos. L-12.
* En autobús: Autobuses Blas & CIA  L-522, 523, 525, 526 y 529H
* Coordenadas GPS: Lat: 40.335913308819116    Lng: -3.8732171058654785
* [Información detallada](https://www.urjc.es/universidad/campus/campus-de-mostoles/564-situacion-planos-campus-mostoles)

### Campus de Fuenlabrada

* Dirección: Camino del Molino s/n. 28943 Fuenlabrada – Madrid
* En tren: Estación Fuenlabrada C-5. RENFE Cercanías 
* En Metro: Estación Hospital de Fuenlabrada L-12.
* En autobús: Autobuses Blas & CIA  Avenida de Atenas L-1, 2, 3, 4, 5 y 6 
* Coordenadas GPS: Lat: 40.28194999999999 Lng: -3.819430000000011
* [Información detallada](https://www.urjc.es/universidad/campus/campus-de-fuenlabrada/562-situacion-planos-campus-fuenlabrada)

### Campus de Vicálvaro

* Dirección: Paseo de los Artilleros s/n. 28032 - Vicálvaro - Madrid
* En tren: Estación Vicálvaro C-2, C-7. RENFE Cercanías 
* En Metro: Estación Vicálvaro L-9
* En autobús(EMT):
    * Calle Daroca: 4, 100, 106, 130, N7. Calle Daroca-Camino Viejo: 4, 100, 106, 130
    * Calle Artilleros-S. Cipriano: 4, 106, N7, E3. Calle Calahorra: 4, 106, 130, N7
* [Información detallada](https://www.urjc.es/universidad/campus/campus-de-madrid/563-situacion-planos-campus-madrid)

## Alojamiento

Los tres campus que se proponen están suficientemente cercanos a las zonas turísticas
o comerciales de Madrid como para que sea perfectamente posible utilizar su capacidad
hotelera como alojamiento. Madrid dispone de varias decenas de miles de plazas de hotel,
y muchas más en pisos compartidos.

A continuación se indican algunos datos más concretos relativos a los tres campus que se ofrecen.

### Campus de Móstoles

Cerca del campus se encuentra el [Hotel Ciudad de Móstoles](https://www.hotelciudaddemostoles.es/?gclid=CjwKCAjw04vpBRB3EiwA0Iieap6JgdpE9duEnb1Uu3-BWz3C8d0wiRDr4NUZ1Qxw5RzeXu3LjYubdRoCAqUQAvD_BwE),
con un precio entre 60 y 80 euros la noche en habitación individual.

El [Hotel La Princesa](https://www.laprincesa.com/), también en Móstoles, cuesta entre 50 y 70.

En Alcorcón (a tres o cuatro paradas de metro) hay alojamientos algo mas baratos,
oscilando los 40 y los 50 euros por noche y encontrándose a una distancia relativamente cercana del campus de Móstoles.
En otros pueblos interconectados por MetroSur (Fuenlabrada, Getafe, Leganés) hay
una amplia oferta hotelera, y el desplazamiento desde el centro de Madrid es relativamente corto.

### Campus de Fuenlabrada

El hotel mas cercano al campus es el [LCB Fuenlabrada](https://www.lcbhoteles.es/), con un precio entre 50 y 60€, el resto de hoteles del barrio están mas lejos pero son algo mas baratos.
En otros pueblos interconectados por MetroSur (Fuenlabrada, Móstoles, Alcorcón, Getafe, Leganés) hay
una amplia oferta hotelera, y el desplazamiento desde el centro de Madrid es relativamente corto.

### Campus de Vicálvaro

En Vicálvaro se encuentra el [Apartahotel Encasa](https://www.aparthotelencasa.es/),
con habitaciones en torno a los 100 euros por noche.
La zona del aeropuerto, y el centro de Madrid, están razonablemente cerca,
con una gran oferta hotelera.


## Otra información

> **Relación con otras comunidades tecnológicas y/o divulgativas que pudieran participar:**
>
> Además de las asociaciones de Estudiantes y las Escuelas de Ingeniería que participan
> en la propuesta, se cuenta en el apoyo de Wikimedia España para ampliar el
> ámbito de esLibre a áreas menos tecnológicas (cultura libre en general), si la
> organización de esLibre lo considera adecuado. Los contactos de los coordinadores
> principales aseguran la involucración de otras comunidades tecnológicas y divulgativas.
> En particular, se explorará la asistencia de alumnos de Educación Secundaria,
> a través de una red de centros de Educación Secundaria con interés en software libre
> de la Comunidad de Madrid, y de otras Universidades madrileñas, vía sus asociaciones
> de estudiantes, sus escuelas técnicas, y sus oficinas relacionadas con software libre.

> **Cómo se gestionaran las solicitudes de ponencias y las inscripciones de los asistentes:**
>
> El proceso de recepción de propuestas de ponencias, y su selección, se hará de forma
> similar a como se realizó en esLibre 2019, siguiendo un proceso basado en "merge requests"
> en GitLab. Como uno de los coordinadores de esta propuesta participó activamente
> durante ese proceso, está asegurada su correcta puesta en marcha y funcionamiento.
>
> Para gestionar inscripciones se utilizara, si la organización de esLibre lo estima conveniente,
> el [sitio de gestión de eventos de la URJC](https://eventos.urjc.es/), que permite
> la publicidad del evento, la inscripción en el mismo, la gestión de inscripciones
> y comunicación con los inscritos, y en su caso, la emisión de certificados de asistencia.
> El equipo coordinador de esta propuesta tiene buena experiencia con esta herramienta,
> pues la ha utilizado en numerosas ocasiones para la organización de eventos de la OfiLibre.
> Su utilización previa en eventos de gran número de asistentes (como las Jornadas de Innovación Docente,
> con varios cientos de asistentes) asegura su escalabilidad para las necesidades del congreso.

> **Posible conciliación de familias asistentes al congreso:**
>
> Se planificarán algunas actividades para niños, en general en el ámbito tecnológico.
> Se tratará también de organizar un espacio para que los niños que no tengan la
> edad para disfrutar de estas actividades antes citadas puedan estar atendidos,
> aunque en el momento de escribir esta propuesta aún no sabemos si vamos a disponer
> de voluntarios suficientes para mantenerlo con garantías.

> [Sí] Apoyo de alguna institución (indicar cual):
>
> * Universidad Rey Juan Carlos, vía la OfiLibre, y las ETSII y ETSIT.
> * Asociaciones de estudiantes citadas en esta propuesta
> * Wikimedia España, para extender las actividades hacia el ámbito de la Cultura Libre

> [Sí] Planes para actividades comunitarias fuera del horario del congreso (comida/cena de la comunidad, ruta turística/culturar, salida nocturna...):
>
> Se facilitará la comida, como se ha indicado en el apartado correspondiente.
> Se organizará una cena de comunidad, el viernes, con inscripción previa y pago por cada
> asistente. Se organizará también para esa misma tarde del viernes, una visita
> a algún lugar de interés, probablemente en el centro de Madrid, si es posible
> combinándola con una ruta de tapeo.
> Es muy posible que se organice una visita del domingo a algún lugar cercano
> a Madrid.

> [Sí] Nos comprometemos a hacer un informe de valoración post-evento, que además de servir para conocer la evolución del mismo, pueda ayudar a futuros equipos organizadores.

## Condiciones

> [Sí] Aceptamos seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.

Se hará lo posible para que todas las presentaciones, talleres y otras actividades
estén centradas en el software libre (o más genéricamente, en tecnologías libres
y cultura libre). Para evitar ambigüedades, se entenderá por software libre el que se distribuye
mediante una licencia que cumple la [Open Source Definition](https://opensource.org/osd)
o las [Debian Free Software Guidelines](https://www.debian.org/social_contract).
Igualmente, se entenderá por cultura libre la que sea distribuida de acuerdo con
la [Free Cultural Works Definition](https://freedomdefined.org/Definition).

En la medida de lo posible, se utilizará software libre para la organización del congreso
(aunque desgraciadamente alguno de los equipos que se usen, especialmente los que
forman parte de la infraestructura de la URJC, van a incluir software no libre).

Todos los materiales que se generen durante la organización del congreso, salvo
los que puedan incluir información confidencial o con implicaciones sobre la privacidad de las personas,
será distribuida con licencias de cultura libre. 