# Coordinación

Este es el proyecto para la coordinación **esLibre**, el congreso sobre tecnologías libres y conocimiento abierto.

Desde aquí se gestionan las propuestas de sedes para las diferentes ediciones. Puedes encontrar la información sobre las ediciones anteriores del [2019](https://eslib.re/2019/), organizada de forma presencial en Granada por [LibreLabGRX](https://librelabgrx.cc/), y la del [2020](https://eslib.re/2020/), organizada de forma virtual desde Madrid por [OfiLibre UJRC](https://ofilibre.gitlab.io/).

Se libre de participar en cualquier parte, tanto en los issues que se abran para discutir y coordinar en el [grupo abierto de Telegram](https://t.me/esLibre), que también está enlazado con el canal de Matrix #esLibre:matrix.org.

También puedes escribirnos por correo electrónico a [hola@eslib.re](mailto:hola@eslib.re).

Actualmente está abierta la **["Convocatoria de Sede" para la edición del año 2021](propuestas/2021/README.md)**.
