Notas de la reunión 2020-09-03

Estamos: Germán, Rafa, Jesús.

* Esquema horario

10:00-10:50 Bienvenida, Keynote, etc.
11:00-12:15 Charlas / Salas
12:15-12:30 Descanso
12:30-13:45 Charlas / Salas 
13:45-15:00 Comida
15:00-15:45 Keynote
16:00-17:15 Charlas / Salas
17:15-17:30 Descanso
17:30-18:45 Charlas / Salas
19:00-19:45 Evento social (viernes) / charla abierta (futuro de esLibre, comunidad de SL en España.. y clausura) (sábado)

* Charlas "normales": 20 min más 5 de preguntas

* 4 ponencias invitadas (keynote)

* Tenemos: 25 charlas largas, 2 cortas, 6 talleres.

* Cada día, por tanto, cuatro sesiones de 1:15 horas en cada linea (track). En cada sesión caben 3 charlas largas, dos largas y dos corts, o un taller. Puede haber sesiones en paralelo, cada una en una línea.

* Por lo tanto, necesitamos 6 sesiones para talleres, 8 sesiones para charlas largas, una para charlas largas y cortas. Total, 15 sesiones. Eso son cuatro líneas (no completas).

Posibles ponentes invitados (proponemos invitarles en este orden):
    
* Lydia Pintscher (KDE, Wikidata)
* Roberto Di Cosmo (Software Heritage)
* Amaya Rodrigo (Debian)
* Daniel German / Kate Stewart (Linux History)
* Maddog Hall ?

* En la recámara: charla de Italo sobre LibreOffice (propuesta como charla larga)

Informar sobre estado despliegue: BBB desplegado y parace que funcionando bien. RocketChat desplegado la semana que viene.

* Tendrimaos que tener una prueba la semana que viene, cuando esté RocketChat. Podría ser el día 9

* Sesión de pruebas con ponentes y coordinadores de sesión: 14 mañana y 15 tarde.

* Necesitarmos como 10 coluntarios para coordinar cada sesión.

Acciones:
    
* Antonio desplegará RocketChat
* Germán escribirá un blog post con la estructura de horarios, y comunicará con las salas para confirmales, explicarles los huecos exactos.
* Jesús o Germán harán una primera propuesta de horario detallado
* Jesús empezará a contactar con posibles ponentes invitados
* Cualquiera: proponer más ponentes invitados. Por favor, tened en cuenta diversidad de género, geográfica, temática, etc.
+ Cualquiera: proponer (o proponerse) como voluntrio para coordinar una sesión.
